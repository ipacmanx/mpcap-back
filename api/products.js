const express = require('express')
const router = express.Router()

const Product = require('../models/Product')

router.get('/products', (req, res) => {
    Product.query()
        .orderBy('sheetId')
        .then(products => {
            res.json(products)
        })
})


async function addOrUpdate(data) {
    const update = await Product.query()
        .where('mscode', data.mscode)

    let id;
    let result;

    if (update.length > 0) {
        id = update[0].id;
        let item = {
            sheetId: data.sheetId,
            wbId: data.wbId,
            ozonId: data.ozonId,
            title: data.title,
        };
        result = await Product.query().findById(id).patch(item);
        console.log('updated');
    } else {
        let item = {
            sheetId: data.sheetId,
            wbId: data.wbId,
            ozonId: data.ozonId,
            title: data.title,
            mscode: data.mscode,
        };
        console.log(item);
        result = await Product.query().insert(item);
        id = result.id;
        console.log('insert new');
    }

    let product = await Product.query().findById(id);

    if (data.price) {
        let last = await product.$relatedQuery('prices').orderBy('created_at').limit(1);
        if (last[0].price != data.price) {
            let price = {
                product_id: id,
                price: data.price,
            };
            let result = await product.$relatedQuery('prices').insert(price);
            console.log(result);
        }
    }

    if (data.wbRating) {
        let last = await product.$relatedQuery('ratings').where('platform_id', 1).orderBy('created_at').limit(1);
        if (last.length > 0 && last[0].rating != data.wbRating) {
            let rating = {
                product_id: id,
                platform_id: 1,
                rating: data.wbRating,
            };
            let result = await product.$relatedQuery('ratings').insert(rating);
            console.log(result);
        }
    }

    if (data.ozonRating) {
        let last = await product.$relatedQuery('ratings').where('platform_id', 2).orderBy('created_at').limit(1);
        if (last.length > 0 && last[0].rating != data.ozonRating) {
            let rating = {
                product_id: id,
                platform_id: 2,
                rating: data.ozonRating,
            };
            let result = await product.$relatedQuery('ratings').insert(rating);
            console.log(result);
        }
    }

    return result;
}

router.post('/products/addOrUpdate', async (req, res) => {
    let body = req.body.body;
    if (body.length > 0) {
        // let data = {
        //     mscode: 'TESTTEST',
        //     wbId: '432424',
        //     ozonId: '432424',
        //     sheetId: 1,
        //     title: 'Тестовый товар которого нет',
        // };
        let result = [];
        body.forEach(async data => {
            let r = await addOrUpdate(data);
            result.push(r instanceof Product);
        })
        res.json(result);
    } else {
        res.json({error: 'Empty request'});
    }
})

router.get('/products/:id', async (req, res) => {
    let id = parseInt(req.params.id)
    if (isNaN(id)) return res.json({ error: 1 });
    let product = await Product.query().findById(id)
    if (product) {
        let prices = await product.$relatedQuery('prices').orderBy('created_at')
        product.prices = prices;
        let ratings = await product.$relatedQuery('ratings').orderBy('created_at')
        product.ratings = ratings;
        res.json(product);
    } else {
        res.json({error: 1});
    }
})

module.exports = {
    router: router
}
