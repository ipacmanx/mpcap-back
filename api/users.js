const express = require('express')
const router = express.Router()

const User = require('../models/User')

router.get('/users', (req, res) => {
    User.query()
        .joinRelated('messages')
        .then(users => {
            res.json(users)
        })
})

router.get('/users/add', async (req, res) => {
    const result = await User.query()
        .insert({
            username: 'test',
            email: 'mockup@email.com'
        })
    console.log(result instanceof User); // --> true
    res.json(result instanceof User);
})

router.get('/users/:id', (req, res) => {
    let id = parseInt(req.params.id)
    User.query()
        .where('id', id)
        .eager('messages')
        .then(user => {
            res.json(user)
        })
})

module.exports = {
    router: router
}
