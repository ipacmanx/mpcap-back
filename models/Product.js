const { Model } = require('objection');
const knex = require('../db/knex')

Model.knex(knex)

class Product extends Model {
    static get tableName() {
        return 'products';
    }

    static get relationMappings() {
        const Price = require('./Price')
        const Rating = require('./Rating')
        return {
            prices: {
                relation: Model.HasManyRelation,
                modelClass: Price,
                join: {
                    from: 'products.id',
                    to: 'prices.product_id'
                }
            },
            ratings: {
                relation: Model.HasManyRelation,
                modelClass: Rating,
                join: {
                    from: 'products.id',
                    to: 'ratings.product_id'
                }
            },
        }
    }
}

module.exports = Product;