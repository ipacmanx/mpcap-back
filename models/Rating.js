const { Model } = require('objection');
const knex = require('../db/knex')

Model.knex(knex)

class Rating extends Model {
    static get tableName() {
        return 'ratings';
    }

    static get relationMappings() {
        const Product = require('./Product')
        return {
            product: {
                relation: Model.BelongsToOneRelation,
                modelClass: Product,
                join: {
                    from: 'ratings.product_id',
                    to: 'products.id'
                }
            }
        }
    }
}

module.exports = Rating;