const { Model } = require('objection');
const knex = require('../db/knex')

Model.knex(knex)

class Price extends Model {
    static get tableName() {
        return 'prices';
    }

    static get relationMappings() {
        const Product = require('./Product')
        return {
            product: {
                relation: Model.BelongsToOneRelation,
                modelClass: Product,
                join: {
                    from: 'prices.product_id',
                    to: 'products.id'
                }
            }
        }
    }
}

module.exports = Price;